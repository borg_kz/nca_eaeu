﻿using Microsoft.Extensions.Configuration;
using NCA.EAEU.Core.GenerateOutputXml;
using System;
using System.IO;
using System.Xml;

namespace EAEU.GenerateXml.Cmd {
	class Program {
		private static string ConnStr = string.Empty;

		public static void Main(string[] args) {
			// if (args.Length == 1) { throw new Exception("Path to xml template not found."); }
			var xmlTemplate = "EDocTemplate.xml"; //args.Length == 1 args[0];
												  // ReadConfiguration();
												  // Create XML reader settings
			XmlReaderSettings settings = new XmlReaderSettings();
			settings.IgnoreComments = true;                         // Exclude comments
			settings.DtdProcessing = DtdProcessing.Ignore;
			settings.ValidationType = ValidationType.DTD;           // Validation

			// Create reader based on settings
			XmlReader reader = XmlReader.Create(xmlTemplate, settings);

			var xml = new XmlDocument();
			xml.Load(reader);
			TemplateManager tm = new TemplateManager();
			tm.Parse(xml);
			ReadConfiguration();
			var results = tm.FillData("1", ConnStr);
			string result = string.Empty;
			int count = 0;
			foreach (var item in results) {
				result = item.GenerateOutputXml();
				File.WriteAllText($"Output{count++} {DateTime.Now.ToString("dd.MM.yyyy")} - Result.xml", result);
			}
			
			Console.Write($"\nФайл сформированы Output {DateTime.Now.ToString("dd.MM.yyyy")}.xml нажмите любую клавишу... ");
			Console.ReadKey();
		}

		private static void ReadConfiguration() {
			// Adding JSON file into IConfiguration.
			var config = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				 .AddJsonFile("appsettings.json", true, true)
				 .Build();

			ConnStr = config.GetSection("ConnectionStrings")["DefaultConnection"];
		}
	}
}
