﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace NCA.EAEU.Core.Managers {
	public class DataManagerBase {
		protected string ConnectionString;

		protected DataManagerBase(IConfiguration configuration) {
			ConnectionString = configuration.GetConnectionString("DefaultConnection");
		}
	}
}
