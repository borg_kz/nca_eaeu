﻿namespace NCA.EAEU.Core {
	public static class Enums {
		public enum EDocHeadertatus {
			None = 0,
			New = 1,
			Processed = 2,
			Failed = 3,
			Suspended = 4,
			Deleted = 5,
			ReadyToSend = 6,
			Sent = 7,
			Canceled = 8
		}

		public enum ERelationType {
			None = 0,
			OneToOne = 1,
			OneToManySame_Table = 2,
			OneToManyRel_Table = 3
		}
	}
}
