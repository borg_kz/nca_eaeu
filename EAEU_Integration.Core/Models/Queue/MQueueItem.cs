﻿using System;

namespace NCA.EAEU.Core.Models {
	public class MQueueItem {
		public int Uid { get; set; }
		public Guid Gid { get; set; }
		public DateTime CreateTime { get; set; }
		public DateTime UpdateTime { get; set; }
		public DateTime PrevUpdateTime { get; set; }
		public Enums.EDocHeadertatus Status { get; set; }
		public string InfEnvelopeCode { get; set; }

		public string EDocCode { get; set; }
		public string EDocId { get; set; }
		public string EDocRefId { get; set; }
		public string EDocDateTime { get; set; }
	}
}
