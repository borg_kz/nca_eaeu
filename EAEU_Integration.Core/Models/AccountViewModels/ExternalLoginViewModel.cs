using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NCA.EAEU.Models.AccountViewModels {
	public class ExternalLoginViewModel {
		[Required]
		[EmailAddress]
		public string Email { get; set; }
	}
}
