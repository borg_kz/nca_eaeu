﻿using NCA.EAEU.Core.GenerateOutputXml.Struct;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace NCA.EAEU.Core.GenerateOutputXml {
	internal class TemplateParser {
		private static Dictionary<string, XTVersion> _versions = new Dictionary<string, XTVersion>();

		public Dictionary<string, XTVersion> Parse(XmlDocument doc) {
			XmlNodeList versions = doc.GetElementsByTagName("Version");
			foreach (XmlNode version in versions) {
				if (version.Attributes["id"].Value is string id) {
					var ver = new XTVersion() { Id = id };
					ParseTemplate(ver, version.FirstChild);
					_versions.Add(id, ver);
				}
			}

			return _versions;
		}

		private void ParseTemplate(XTVersion ver, XmlNode version) {
			var template = new XTHeadNode() { Node = version.Name };
			ver.Template = template;
			HandleXmlNode(template, version.ChildNodes);
		}

		private void HandleXmlNode(XTNodeContainerBase<IXmlTemplate> head, XmlNodeList nodes) {
			foreach (XmlNode item in nodes) {
				if (item.HasChildNodes && item.FirstChild.Value == null) {
					if (item.Attributes.Count > 0 && item.Attributes["map"]?.Value is string map) {
						var innerTable = new XTTableNode() {
							Node = item.Name,
							Parent = head,
							Map = map,
							IsMain = item.Attributes["main"]?.Value.ToLower() == "true",
							RelationType = (Enums.ERelationType)Enum.Parse(typeof(Enums.ERelationType), item.Attributes["relationType"]?.Value ?? Enums.ERelationType.OneToOne.ToString()),
							
							Relation = item.Attributes["relation"]?.Value ?? string.Empty
						};

						head.Items.Add(innerTable);
						HandleXmlNode(innerTable, item.ChildNodes);
					} else {
						var innerTable = new XTHeadNode() {
							Node = item.Name,
							Parent = head
						};
						for (int i = 0; i < item.Attributes.Count; i++)
							innerTable.Attributes.Add((item.Attributes[i].Name, item.Attributes[i].Value));

						head.Items.Add(innerTable);
						HandleXmlNode(innerTable, item.ChildNodes);
					}
				} else {
					XTDataNode node = null;
					if (item.Attributes["relation"] != null)
						node = new XTListDataNode() { Relation = item.Attributes["relation"].Value };
					else node = new XTDataNode();
					node.Node = item.Name;
					node.DbPath = item.InnerText;
					node.Parent = head;
					foreach (XmlAttribute attrebute in item.Attributes)
						node.Attributes.Add((Key: attrebute.Name, Value: attrebute.Value));
					
					head.Items.Add(node);
				}
			}
		}
	}
}
