﻿using NCA.EAEU.Core.GenerateOutputXml.Struct;
using System;
using System.Collections.Generic;
using System.Text;

namespace NCA.EAEU.Core.GenerateOutputXml.Struct {
	public class XTVersion {
		public string Id { get; set; }
		public XTHeadNode Template = null;
	}
}
