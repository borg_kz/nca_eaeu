﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace NCA.EAEU.Core.GenerateOutputXml.Struct {
	public class XTTableNode : XTNodeContainerBase<IXmlTemplate> {
		private string _relation = string.Empty;

		public string Map { get; set; }
		public Enums.ERelationType RelationType { get; set; }
		public string Relation {
			get {
				return _relation;
			}
			set {
				_relation = value;
				IsRelationTable = _relation.StartsWith("Rel_");
			}
		}

		public bool IsMain { get; set; }
		public bool IsRelationTable { get; set; }

		// [MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Obsolete]
		public string Table {
			get {
				var strs = Map.TrimStart('#');
				return Map.TrimStart('#');
			}
		}

		public (string Table, string ForegionKey) SplitRelation {
			get {
				var strs = Relation.Split('.');
				return (strs[0], strs[1]);
			}
		}

		public override IXmlTemplate Copy() {
			return new XTTableNode() { Node = Node, Attributes = Attributes };
		}
	}
}
