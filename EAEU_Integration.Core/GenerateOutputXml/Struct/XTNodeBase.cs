﻿using NCA.EAEU.Core.GenerateOutputXml.Struct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NCA.EAEU.Core.GenerateOutputXml.Struct {
	public abstract class XTNodeBase : IXmlTemplate {
		public List<(string Key, string Value)> Attributes { get; set; } = new List<(string Key, string Value)>();
		public string Node { get; set; }
		public IXmlTemplate Parent { get; set; }

		public abstract IXmlTemplate Copy();
		public abstract string GenerateOutputXml();

		public override string ToString() {
			return Node;
		}
	}	
}