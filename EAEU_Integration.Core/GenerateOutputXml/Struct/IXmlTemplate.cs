﻿using System.Collections.Generic;

namespace NCA.EAEU.Core.GenerateOutputXml.Struct {
	public interface IXmlTemplate {
		List<(string Key, string Value)> Attributes { get; set; }
		string Node { get; set; }
		string GenerateOutputXml();
		IXmlTemplate Parent { get; set; }

		IXmlTemplate Copy();
	}
}