﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NCA.EAEU.Core.GenerateOutputXml.Struct {
	public abstract class XTNodeContainerBase<T> : IXmlTemplate where T: IXmlTemplate {
		public string Node { get; set; }
		public int Uid { get; set; }
		public List<(string Key, string Value)> Attributes { get; set; } = new List<(string Key, string Value)>();
		public IXmlTemplate Parent { get; set; }

		public List<T> Items { get; set; } = new List<T>();
		public bool IsDataNode { get; } = false;

		public abstract IXmlTemplate Copy();

		public string GenerateOutputXml() {
			return $"\r\n<{Node}>" +
				$"	{Items.Select(a => a.GenerateOutputXml()).Aggregate(new StringBuilder(), (c, n) => c.Append(n))}" +
				$"\r\n</{Node}>";
		}

		public override string ToString() {
			return Node;
		}
	}
}
