﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NCA.EAEU.Core.GenerateOutputXml.Struct {
	public interface IXmlDataNode : IXmlTemplate {
		string DbPath { get; set; }
		string Value { get; set; }
	}
}