﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NCA.EAEU.Core.GenerateOutputXml.Struct
{
	public class XTListDataNode : XTDataNode {
		public string Relation { get; set; }

		public (string Table, string ForegionKey) SplitRelation {
			get {
				var strs = Relation.Split('.');
				return (strs[0], strs[1]);
			}
		}
	}
}
