﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NCA.EAEU.Core.GenerateOutputXml.Struct {
	public class XTHeadNode : XTNodeContainerBase<IXmlTemplate> {
		public override IXmlTemplate Copy() {
			return new XTHeadNode() { Node = Node, Attributes = Attributes };
		}
	}
}
