﻿using System;
using System.Linq;
using System.Text;

namespace NCA.EAEU.Core.GenerateOutputXml.Struct {
	public class XTDataNode : XTNodeBase, IXmlDataNode {
		private string _dbPath = string.Empty;
		public string DbPath {
			get { return _dbPath; }
			set {
				if (!value.StartsWith('#'))
					throw new ArgumentException($"{Node} - Значения для поиска в базе данных задано некорректно: {value}");
				if (value.Split('.').Length != 2)
					throw new ArgumentException($"{Node} - Значения для поиска в базе данных не содержит ТАБЛИЦА.ПОЛЕ: {value}");

				_dbPath = value;
			}
		}
		
		public override IXmlTemplate Copy() {
			return new XTDataNode() { Node = Node, Value = Value, Attributes = Attributes };
		}

		public (string Table, string Field) SplitDbPath {
			get {
				var strs = DbPath.Split('.');
				return (strs[0], strs[1]);
			}
		}

		public override string GenerateOutputXml() {
			if (string.IsNullOrWhiteSpace(Value))
				return string.Empty;

			return $"\r\n	<{Node}{Attributes.Aggregate(new StringBuilder(), (a, b) => a.Append($" {b.Key}=\"{b.Value}\""))}>{Value}</{Node}>";
		}

		public string Value { get; set; }

		public override string ToString() {
			return "<" + base.ToString() + ">" + DbPath;
		}
	}
}
