﻿using NCA.EAEU.Core.GenerateOutputXml.Struct;
using System;
using System.Collections.Generic;
using System.Xml;

namespace NCA.EAEU.Core.GenerateOutputXml {
	public class TemplateManager {
		private static Dictionary<string, XTVersion> _versions = new Dictionary<string, XTVersion>();
		private XTVersion _currentVersion = null;
		public bool Parse(XmlDocument xml) {
			_versions.Clear();
			_versions = new TemplateParser().Parse(xml);
			return true;
		}

		public List<IXmlTemplate> FillData(string version, string connectionString) {
			if (!_versions.ContainsKey(version))
				throw new ArgumentNullException("Заданная версия не найдена: " + version);
			_currentVersion = _versions[version];
			return new TemplateFiller().Fill(_currentVersion, connectionString);
		}
	}
}
