﻿using NCA.EAEU.Core.GenerateOutputXml.Struct;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace NCA.EAEU.Core.GenerateOutputXml {
	public class TemplateFiller {
		public List<IXmlTemplate> Result = new List<IXmlTemplate>();
		private string _emptyForegionKey = "-1";
		private XTVersion _version = null;

		public List<IXmlTemplate> Fill(XTVersion version, string connection) {
			using (SqlConnection con = new SqlConnection(connection))
			using (SqlCommand cmd = new SqlCommand("select Uid from EDocHeader where Status = " + (int)Enums.EDocHeadertatus.ReadyToSend, con)) {
				List<int> uids = new List<int>();
				try {
					con.Open();
					using (SqlDataReader dr = cmd.ExecuteReader()) {
						if (dr.HasRows)
							while (dr.Read())
								uids.Add(dr.GetInt32(0));
						else { 
							Console.WriteLine("В базе данных не найдено данных для генерации.");
							return null;
						}
					}
				} catch (Exception e) {
					Console.WriteLine("Error: " + e.Message);
				}

				_version = version;
				Parallel.ForEach(uids, (uid) => {
					XTHeadNode result = (XTHeadNode)version.Template.Copy();
					Result.Add(result);

					FillTemplate(uid.ToString(), _version.Template, con, result, new Dictionary<string, FKTable>());
				});
			}

			return Result;
		}

		private void FillTemplate(string uid, XTHeadNode version, SqlConnection con, XTHeadNode result, Dictionary<string, FKTable> foregionKeys) {
			foreach (var node in version.Items) {
				var data = new Dictionary<string, string>();
				if (node is XTTableNode table) {
					data = LoadOneToOneData(table.IsMain ? uid : foregionKeys[table.SplitRelation.Table].GetValue(table.SplitRelation.ForegionKey), table.Map, "Uid", con, foregionKeys);
					XTTableNode copyTable = (XTTableNode)table.Copy();
					result.Items.Add(copyTable);
					foreach (var item in table.Items) {
						NodeHandle(item, data, con, copyTable, foregionKeys);
					}
				}
			}
		}

		private void NodeHandle(IXmlTemplate item, Dictionary<string, string> data, SqlConnection con, XTNodeContainerBase<IXmlTemplate> result, Dictionary<string, FKTable> foregionKeys) {
			if (item is XTTableNode table) {
				switch(table.RelationType) {
					case Enums.ERelationType.None: 
					case Enums.ERelationType.OneToOne:
						var childData = LoadOneToOneData(foregionKeys[table.SplitRelation.Table].GetValue(table.SplitRelation.ForegionKey), table.Map, "Uid", con, foregionKeys);
						XTTableNode copyTable = (XTTableNode)table.Copy();
						result.Items.Add(copyTable);
						foreach (var child in table.Items)
							NodeHandle(child, childData, con, copyTable, foregionKeys); 
					break;
					case Enums.ERelationType.OneToManyRel_Table:
						var uids = LoadOneToManyRelData(foregionKeys[((XTTableNode)table.Parent).Map].GetValue("Uid"), table.SplitRelation.Table, table.SplitRelation.ForegionKey, con);
						foreach (var uid in uids) {
							 copyTable = (XTTableNode)table.Copy();
							result.Items.Add(copyTable);
							var tableData = LoadOneToOneData(uid, table.Map, "Uid", con, foregionKeys);
							foreach (var child in table.Items)
								NodeHandle(child, tableData, con, copyTable, foregionKeys);
						}
						break;
					case Enums.ERelationType.OneToManySame_Table:
						uids = LoadOneToManySame(foregionKeys[((XTTableNode)table.Parent).Map].GetValue("Uid"), con, table);
						foreach (var uid in uids) {
							 copyTable = (XTTableNode)table.Copy();
							result.Items.Add(copyTable);
							var tableData = LoadOneToOneData(uid, table.SplitRelation.Table, "Uid", con, foregionKeys);
							foreach (var child in table.Items)
								NodeHandle(child, tableData, con, copyTable, foregionKeys);
						}
					break;
				}
				
				return;
			}

			if (item is XTListDataNode listDataNode) {
				var list = LoadOneToManyRelData(foregionKeys[((XTTableNode)listDataNode.Parent).Map].GetValue("Uid"), listDataNode.SplitRelation.Table, listDataNode.SplitRelation.ForegionKey, con);

				foreach (var listItem in list) {
					XTDataNode copyDataNode = (XTDataNode)listDataNode.Copy();
					result.Items.Add(copyDataNode);
					copyDataNode.Value = listItem;
					FillAttrebutes(copyDataNode, data, listDataNode);
				}
				return;
			}
			
			if (item is XTDataNode dataNode) {
				XTDataNode copyDataNode = (XTDataNode)dataNode.Copy();
				result.Items.Add(copyDataNode);
				if (!data.ContainsKey(dataNode.SplitDbPath.Field))
					throw new ArgumentException("Не найдено: " + dataNode.ToString() + " в объекте: " + dataNode.Parent.Node);
				copyDataNode.Value = data[dataNode.SplitDbPath.Field];

				FillAttrebutes(copyDataNode, data, dataNode);
				return;
			}

			if (item is XTHeadNode headNode) {
				XTHeadNode copyTable = (XTHeadNode)headNode.Copy();
				result.Items.Add(copyTable);
				foreach (var child in headNode.Items) {
					NodeHandle(child, data, con, copyTable, foregionKeys);
				}
				return;
			}

			throw new ApplicationException("Непонятного типа херня: " + item.Node);
		}

		private void FillAttrebutes(XTDataNode copyDataNode, Dictionary<string, string> data, IXmlDataNode dataNode) {
			var attrs = new List<(string, string)>();
			for (int i = 0; i < copyDataNode.Attributes.Count; i++) {
				var attrebute = copyDataNode.Attributes[i];
				var strs = attrebute.Value.Split('.');
				if (strs.Length != 2)
					throw new ArgumentException("Значение атрибута не корректно: " + attrebute.Value + " в объекте: " + dataNode.Parent.Node);
				if (data.ContainsKey(strs[1]))
					attrs.Add((attrebute.Key, data[strs[1]]));
			}
			copyDataNode.Attributes = attrs;
		}

		private Dictionary<string, string> LoadOneToOneData(string key, string table, string field, SqlConnection con, Dictionary<string, FKTable> foregionKeys) {
			if (key == _emptyForegionKey)
				throw new ArgumentException("UId не задан -1: " + table);

			using (SqlCommand cmd = new SqlCommand($"select * from {table} where {field} = {key}", con))
			using (SqlDataReader dr = cmd.ExecuteReader()) {
				if (!dr.HasRows)
					throw new InvalidOperationException($"Что то пошло не так. Не найдены данны таблица: {table} {field}: {key}");
				var data = new Dictionary<string, string>();
				while (dr.Read())
					for (int i = 0; i < dr.FieldCount; i++) {
						var value = string.Empty;
						if (!(dr.GetValue(i) is DBNull)) { 
							switch(dr.GetColumnSchema()[i].DataTypeName) {
								case "date": value = dr.GetDateTime(i).ToString("yyyy-MM-dd"); break;
								case "datetime": value = dr.GetDateTime(i).ToString("yyyy-MM-ddThh:mm:ssZ"); break;
								case "bit": value = dr.GetValue(i).ToString().ToLower(); break;
								default: value = dr.GetValue(i).ToString(); break;
							}
						}

						string column = dr.GetColumnSchema()[i].ColumnName;
						data.Add(column, value);

						if (column.StartsWith("FK_") || column == "Uid")
							if (!foregionKeys.ContainsKey(table)) {
								var temp = new FKTable() { Table = table };
								temp.SetValue(column, value);
								foregionKeys.Add(table, temp);
							} else
								foregionKeys[table].SetValue(column, value);
					}
				return data;
			}
		}

		private List<string> LoadOneToManyRelData(string uid, string table, string foregionKey, SqlConnection con) {
			if (uid == _emptyForegionKey)
				throw new ArgumentException("UId не задан -1: " + table + ", " + foregionKey);

			using (SqlCommand cmd = new SqlCommand($"select * from {table} where {foregionKey} = {uid}", con))
			using (SqlDataReader dr = cmd.ExecuteReader()) {
				if (!dr.HasRows)
					return new List<string>();
				var data = new List<string>();
				while (dr.Read())
					for (int i = 0; i < dr.FieldCount; i++) {
						string column = dr.GetColumnSchema()[i].ColumnName;
						string value = dr.GetValue(i).ToString();
						if (column != foregionKey)
							data.Add(value);
					}

				return data;
			}
		}

		// Можно оптимизировать и возращать сразу весь набор данных
		private List<string> LoadOneToManySame(string uid, SqlConnection con, XTTableNode table) {
			if (uid == _emptyForegionKey)
				throw new ArgumentException("UId не задан -1: " + table.Node);

			using (SqlCommand cmd = new SqlCommand($"select uid from {table.Map} where {table.SplitRelation.ForegionKey} = {uid}", con))
			using (SqlDataReader dr = cmd.ExecuteReader()) {
				if (!dr.HasRows)
					return new List<string>();
				var result = new List<string>();
				while (dr.Read())
					result.Add(dr.GetValue(0).ToString());

				return result;
			}
		}

		private class FKTable {
			public string Table { get; set; }
			private Dictionary<string, string> _keys = new Dictionary<string, string>();

			public string GetValue(string key) {
				return _keys[key];
			}

			public void SetKey(string key) {
				_keys[key] = "-1";
			}

			public void SetValue(string key, string value) {
				_keys[key] = value;
			}
		}
	}
}
